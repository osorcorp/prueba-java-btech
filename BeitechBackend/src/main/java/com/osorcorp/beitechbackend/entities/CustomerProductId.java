/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.osorcorp.beitechbackend.entities;

import java.io.Serializable;


/**
 *
 * @author oscar
 */
public class CustomerProductId implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Customer customer;
	private Product product;	
	
	public CustomerProductId() {
	
}

	public CustomerProductId(Customer customer, Product product) {
		this.customer = customer;
		this.product = product;
	}
	
	@Override
	public int hashCode() {
		final long prime = 31;
		long result = 1;
		result = prime * result + customer.getCustomerId();
		result = prime * result + product.getProductId();
		return (int)result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CustomerProductId other = (CustomerProductId) obj;
		if (customer != other.customer)
			return false;
		if (product != other.product)
			return false;
		return true;
	}
	
}
