import { Component } from '@angular/core';

import { Order } from '../entities/Order';
import { Customer } from '../entities/Customer';
import { Product } from '../entities/Product';
import { APIService } from '../api.service';
import { OrderDetail } from '../entities/OrderDetail';
import { formatDate } from '@angular/common';

@Component({
  selector: 'app-order-form',
  templateUrl: './Order-form.component.html',
  styleUrls: ['./Order-form.component.css']
})
export class OrderFormComponent {

  products: Product[];
  customers: Customer[];
  customer: Customer;
  customerOrder: Order;
  orderDetails: OrderDetail[];
  submitted = false;
  total = '';
  date = new Date();

  constructor(private customerService: APIService) {
  }

  model = new Order(1, String(this.date),'Calle Falsa 123','1', this.orderDetails);


  ngOnInit() {
    this.getCustomers();
  }

  onChangeCustomer(): void {
    this.customerService.getCustomerAvailableProducts(this.model['customer']['customerId'])
      .subscribe(products => this.products = products);
  }

  onSelectProduct(): void {
    this.model.total = this.total = this.model['orderDetails']['product']['price'];
  }

  getCustomers(): void {
    this.customerService.getCustomers()
      .subscribe(customers => this.customers = customers);
  }



  onSubmit() { this.submitted = true; console.log(this); this.newOrder()}

  newOrder() {
    console.log("guardando");   
    let date = new Date();
    this.model.creationDate = String(formatDate(date,'yyyy-MM-dd','en'))
    let array = [{
      "orderDetailId": null,
      "quantity": 1,
      "price" : this.model['orderDetails']['product']['price'],
      "product" : this.model['orderDetails']['product'],
      "productDescription" : this.model['orderDetails']['product']['productDescription'],
      "name" : this.model['orderDetails']['product']['name']
    }]

    let envio = {
      "orderId":null,
      "creationDate": this.model.creationDate,
      "deliveryAddress": this.model.deliveryAddress,
      "total": this.model.total,
      "customer": this.model['customer'],
      "orderDetails": array
    }
    console.log(envio);
    //console.log({"creationDate":"2019-08-25", "total":"4500","customer":{"name":"laura acosta","email":"laura_acosta@testbeitech.com","customerId":2},"deliveryAddress":"calle falsa 123","orderDetails":[{"product":{"productId":"13","name":"producto m","productDescription":"descripcion del producto m","price":4500},"name":"producto m","productDescription":"descripcion del producto m","price":4500,"quantity":1}]})
    this.customerService.createOrder(envio).subscribe( data => {
      alert("Order created successfully.");
    });
  }
}