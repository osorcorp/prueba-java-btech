import { OrderDetail } from './OrderDetail';

export class Order {
	orderId: Number;
	creationDate: String;	
	deliveryAddress: String;
	total: String;
	orderDetails: OrderDetail[];


	constructor(
		orderId: Number,
		creationDate: String,
		deliveryAddress: String,
		total: String,
		ordersDetail: OrderDetail[]) { }
}