import { Product } from './Product';

export class OrderDetail {
	orderDetailId: number;
	quantity: number;
	price : number;
	name: String;
	productDescription: String;
	product : Product;

	constructor(
		orderDetailId: number,
		quantity: number,
		price : number,
		name: String,
		productDescription: String,
		product : Product) { }
}