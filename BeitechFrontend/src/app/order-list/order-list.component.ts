import { Component } from '@angular/core';

import { Customer } from '../entities/Customer';
import { Order } from '../entities/Order';
import { APIService } from '../api.service';
import { ActivatedRoute } from '@angular/router';




@Component({
  selector: 'app-order-list',
  templateUrl: './order-list.component.html',
  styleUrls: ['./order-list.component.css']
})
export class OrderListComponent {
  	title = 'Customer orders';
    customerOrders: Order[];
    id: string;
    name: string;

    constructor(private customerService: APIService,private activatedRoute: ActivatedRoute) {
	  }

	ngOnInit() {
		this.activatedRoute.params.subscribe(paramsId => {
      this.id = paramsId.id;
      this.name = paramsId.name;
    });
    console.log(this.id);
    this.onChangeCustomer();
  }
  
  onChangeCustomer(){
    this.customerService.getOrders(this.id)
        .subscribe(customerOrders => this.customerOrders = customerOrders);
  }

}
